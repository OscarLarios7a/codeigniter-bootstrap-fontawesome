<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examples extends CI_Controller {
	/*
	Load the template_view view, and add the title, the body and footer.
	*/
	public function index()
	{
		$data ['title']		= "-- Title Page --";
		$data ['header']	= "header/header_view";
		$data ['content']	= "example_view";
		$data ['footer']	= "footer/footer_view";
		$this->load->view('template_view', $data);
	}

}

/* End of file Examples.php */
/* Location: .//C/xampp/htdocs/code-help/codeigniter/controller/Examples.php */
